<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Teachers Assessment</title>
  @include('partials.styles')
  <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
</head>

<body>
  <div class="container-scroller">
    @include('partials.header')
    <div class="container-fluid page-body-wrapper">
     @include('partials.navbar')
     @yield('content')
    </div>
  </div>
   @include('partials.scripts')
</body>

</html>