<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Teachers Assement Software</title>
  
   @include('partials.styles')

   <link rel="shortcut icon" href="{{ asset('/images/favicon.ico')}}">
</head>

<body>

       <div class="row">
         <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
          
            <div class="container text-left">
                <h4>
                  <a href="{{ route('index') }}" class="nav-link" style="color:white"><b>Teachers Assessment Software</b>
                  </a>
                </h4>
            </div>

        </nav>
       </div>
         
       <div class="container" style="margin-bottom:50px; margin-top:60px">

          <div class="row">

           <div class="col-lg-2 col-md-2 col-sm-1 col-xs-12"></div>
           <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
             
              <div class="card">
                 <div class="card-header">
                    <h4 class="text-center">Permission Not Allowed</h4>
                 </div>
                 <div class="card-body" style="margin:-30px; height:500px">
                    <h5 class="text-center" style="margin-top: 200px">
                    	<a href="{{ route('index') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
                    </h5>
                 </div>
                 <div class="card-footer text-center">
                  <div class="container text-center">
		               <span>Copyright © {{ date('Y') }}
		               <a href="http://www.trinitysoftwarecenter.com/" target="_blank">Trinity Software Center</a>. All Rights Reserved</span>
		          </div>
                 </div>
              </div>

           </div>
           <div class="col-lg-2 col-md-2 col-sm-1 col-xs-12"></div>

        </div>

       @include('partials/scripts')
      
</body>

</html>
