<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Teachers Assement Software</title>

   @include('partials.styles')

   <link rel="shortcut icon" href="{{ asset('/images/favicon.ico')}}">
</head>

<body>

       <div class="row">
         <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">

            <div class="container text-left">
                <h4>
                  <a href="{{ route('index') }}" class="nav-link" style="color:white"><b>Teachers Assessment Software</b>
                  </a>
                </h4>
            </div>

        </nav>
       </div>

       <div class="container" style="margin-bottom:50px; margin-top:60px">

          @include('partials.alert')



          <div class="row">



           <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

            <div class="alert alert-success">
                <h3 class="text-center">For Complete School Management System For Your School <br> <i class="fa fa-arrow-right" title="Click here to visit Skuuni Management System"></i> CLICK <a href="http://www.skuuni.com" target="_blank">WWW.SKUUNI.COM</a></h3>
            </div>


            @foreach($articles as $article)

              <div class="card">
                 <div class="card-header">
                    <h2>{{ $article->title }}</h2>
                 </div>
                 <div class="card-body" style="margin:-30px">
                  <img class="img-fluid img-thumbnail" src="{{ asset('images/software/'.$article->avatar) }}">
                  <p>{{ $article->body }}</p>
                 </div>
                 <div class="card-footer text-center">
                   <a href="{{ $article->link }}" target="_blank"><i>Click to watch guide on YouTube</i></a>
                 </div>
              </div><hr>

             @endforeach

           </div>

           <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
             <div class="alert alert-dark">
                <a href="" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">Register Product</a>
             </div>


             <div class="alert alert-dark">
                <p><b>Click any of the button to download version 1.0.0</b></p>
                <a href="{{ route('setup_download') }}" class="btn btn-success btn-block">Download for windows 64</a> <br>
                <a href="{{ route('setup_download_two') }}" class="btn btn-primary btn-block">Download for windows 32</a>
             </div>

              <div class="alert alert-dark">
                 <p><b>Comments are welcome</b></p><hr>
                 <form role="form" action="{{ route('save_comment') }}" method="post">
                     {{ csrf_field() }}
                    <div class="form-group">
                      <label for="features">Features</label>
                      <input type="text" name="features" class="form-control" id="features" required placeholder="Eg. Assessment Report"  required>
                    </div>

                    <div class="form-group">
                      <label for="comment">Comment</label>
                      <textarea rows="5" cols="2" name="comment" id="comment" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> Submit</button>
                    </div>

                 </form>
             </div>

           </div>

           </div>

        </div>

        <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

          <!-- Modal content-->
          <div class="modal-content" style="background-color:white">
          <form role="form" action="{{ route('save_customer') }}" method="post" id="myForm">
             {{ csrf_field() }}
            <div class="modal-header">

              <h4 class="modal-title">
                 Request Registration Key
              </h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                  <div class="form-group">
                    <label for="name">Full Name :</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter Full Name" required>
                  </div>

                  <div class="form-group">
                    <label for="contact">Contact :</label>
                    <input type="text" name="contact" class="form-control" id="contact" required placeholder="Enter Contact Number" maxlength="10" onkeypress="return isNumberKey(event)"  required>
                  </div>

                  <div class="form-group">
                    <label for="email">Email Address:</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email Address" required>
                  </div>

                  <div class="form-group">
                    <label for="usertype">User Type</label>
                    <select class="form-control" name="user_type" id="user_type" required >
                        <option selected="selected">Select User Type</option>
                        <option value="self">Self</option>
                        <option value="school">School</option>
                    </select>
                  </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary btn-block">Submit</button>
            </div>
            </form>
          </div>

        </div>
        </div>

        <div id="myLogin" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

          <!-- Modal Login fdfdfdfdfdf-->
          <div class="modal-content" style="background-color:white">
          <form role="form" action="{{ route('login_user') }}" method="post">
             {{ csrf_field() }}
            <div class="modal-header">

              <h4 class="modal-title">
                Provide Credentials
              </h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                  <div class="form-group">
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email" required>
                  </div>

                  <div class="form-group">
                    <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" required>
                  </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-star"></i> Login</button>
            </div>
            </form>
          </div>

        </div>
        </div>


        <footer class="navbar navbar-default navbar-fixed-bottom">
          <div class="container text-center">
              <span>Copyright <b data-toggle="modal" data-target="#myLogin"> ©</b> {{ date('Y') }}
               <a href="http://www.trinitysoftwarecenter.com/" target="_blank">Trinity Software Center</a>. All Rights Reserved</span>
          </div>
        </footer>

       @include('partials/scripts')


      <script type="text/javascript">

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }


         $(document).ready(function(){
            $("#myForm").on("submit", function(){
               alert('Saved');
            });//submit
          });
      </script>
</body>

</html>
