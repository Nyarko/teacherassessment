@extends('layouts.adminLayout')

@section('content')

<div class="main-panel">
<div class="content-wrapper">
  
  @include('partials.alert')
  <div class="row">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">All Comments</h4>
          <div class="table-responsive">
            <table class="table table-bordered" id="myTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Features</th>
                  <th>Comment</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($comments as $id => $comment)
                  <tr>
                  <td class="font-weight-medium">{{ $id += 1}}</td>
                  <td>{{ ucfirst(trans( $comment->features ))}}</td>
                  <td>{{ $comment->comment }}</td>
                  <td>{{ date('M j, Y', strtotime($comment->created_at )) }}</td>
                  <td style="text-align: center"><a href="{{ route('del_comment', [$id => $comment->id]) }}" class="text-danger" title="click to delete"><i class="fa fa-trash"></i></a></td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('partials.footer')
</div>

@endsection