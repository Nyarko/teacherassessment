@extends('layouts.adminLayout')

@section('content')

<div class="main-panel">
<div class="content-wrapper">

 @include('partials.alert')
  
  <div class="row">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <div class="card-text">
            <tr>
            <td><h4 class="card-title"><i class="fa fa-star"></i> All Articles</h4></td> 
            <td><a href="{{ route('all_user') }}" class="btn btn-success fa-pull-right" data-toggle="modal" data-target="#register_user"><b><i class="fa fa-plus"></i> Add Article</b></a></td><br><br>
            </tr>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered" id="myTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Body</th>
                  <th>Link</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($articles as $id => $article)
                  <tr>
                  <td class="font-weight-medium">{{ $id += 1 }}</td>
                  <td><img class="img-fluid img-circle" src="{{ asset('images/software/'.$article->avatar) }}" width="50px" height="50px"></td>
                  <td>{{ ucfirst(trans( $article->title ))}}</td>
                  <td>{{ str_limit($article->body, $limit = 20, $end = '...') }}</td>
                  <td>{{ $article->link }}</td>
                  <td>{{ date('M j, Y', strtotime($article->created_at )) }}</td>
                  <td style="text-align: center">
                     
                     <a href="#" class="text-success" title="click to view" data-toggle="modal" data-target="#update_article{{ $article->id }}">
                      <i class="fa fa-eye"></i>
                     </a> |

                     <a href="{{ route('del_article', [$id => $article->id]) }}" class="text-danger" title="click to delete">
                      <i class="fa fa-trash"></i>
                     </a>
                  </td>

                 {{-- The update of article model --}}

              <div id="update_article{{ $article->id }}" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                  <div class="modal-content">
                     <form action="{{ route('update_article', [$id => $article->id]) }}" method="post" enctype="multipart/form-data">
                       {{ csrf_field() }}

                       <input type="hidden" name="user_id" id="user_id" value="{{ $article->id }}">

                        <div class="modal-header">
                           <h4 class="modal-title">Update New Article</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                         <div class="modal-body">
                               

                                <div class="form-group">
                                  <img class="img-fluid img-circle" src="{{ asset('images/software/'.$article->avatar) }}" width="70px" height="70px">
                                </div>

                                <div class="form-group">
                                  <div class="input-group">
                                    <input type="text" class="form-control" name="title" value="{{ $article->title }}" required="required">
                                    <div class="input-group-append">
                                      <span class="input-group-text">
                                        <i class="fa fa-circle"></i>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              
                                 <div class="form-group">
                                   <div class="input-group">
                                    <input type="file" class="form-control" name="picture" placeholder="Page photo">
                                    <div class="input-group-append">
                                      <span class="input-group-text">
                                        <i class="fa fa-image"></i>
                                      </span>
                                    </div>
                                   </div>
                                 </div>

                                <div class="form-group">
                                  <div class="input-group">
                                      <textarea rows="5" cols="5" class="form-control" name="body" id="body">{{ $article->body }}</textarea>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="input-group">
                                    <input type="text" class="form-control" name="link" value="{{ $article->link }}" id="link" >
                                    <div class="input-group-append">
                                      <span class="input-group-text">
                                        <i class="fa fa-link"></i>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                  
                          </div>

                          <div class="modal-footer">
                              <button type="submit" class="btn btn-success submit-btn btn-lg btn-block">
                                <i class="menu-icon fas fa-edit"></i> Update
                              </button>
                          </div>

                      </form>
                   </div>
                </div>
              </div>











                 </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="register_user" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
       <form action="{{ route('article_user') }}" method="post" enctype="multipart/form-data">
         {{ csrf_field() }}

          <div class="modal-header">
             <h4 class="modal-title">Add New Article</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

           <div class="modal-body">
              
                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" class="form-control" name="title" placeholder="Enter Title" required="required">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-circle"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                
                   <div class="form-group">
                     <div class="input-group">
                      <input type="file" class="form-control" name="picture" placeholder="Page photo">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-image"></i>
                        </span>
                      </div>
                     </div>
                   </div>

                  <div class="form-group">
                    <div class="input-group">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Enter Description Here" name="body" id="body"></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" class="form-control" name="link" placeholder="Enter Lick to Youtube" id="link" >
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-link"></i>
                        </span>
                      </div>
                    </div>
                  </div>
    
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success submit-btn btn-lg btn-block">
                  <i class="menu-icon fas fa-save"></i> Save
                </button>
            </div>

        </form>
     </div>
  </div>
</div>

@include('partials.footer')
</div>

@endsection

<script type="text/javascript">
 
</script>