@extends('layouts.adminLayout')

@section('content')

<div class="main-panel">
<div class="content-wrapper">

 @include('partials.alert')
  
  <div class="row">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <div class="card-text">
            <tr>
            <td><h4 class="card-title"><i class="fa fa-star"></i> All Requests</h4></td> 
            <td><a href="{{ route('all_user') }}" class="btn btn-success fa-pull-right" data-toggle="modal" data-target="#register_user"><b><i class="fa fa-user"></i> Add User</b></a></td><br><br>
            </tr>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered" id="myTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $id => $user)
                  <tr>
                  <td class="font-weight-medium">{{ $id += 1 }}</td>
                  <td>{{ ucfirst(trans( $user->name ))}}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->contact }}</td>
                  <td>{{ date('M j, Y', strtotime($user->created_at )) }}</td>
                  <td style="text-align: center">
                    <a href="{{ route('del_user', [$id => $user->id]) }}" class="text-danger" title="click to delete">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="register_user" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
       <form action="{{ route('save_user') }}" method="post" enctype="multipart/form-data">
         {{ csrf_field() }}

          <div class="modal-header">
             <h4 class="modal-title">Profile Details</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

           <div class="modal-body">
              
                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" class="form-control" name="name" placeholder="Enter Full Name" required="required">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-user"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="email" class="form-control {{ $errors->has('email') ? 'has-error' : '' }}" name="email" placeholder="Enter Email" required="required" id="email">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-envelope"></i>
                        </span>
                      </div>

                      @if ($errors->has('email'))
                      <span class="help-block text-danger">
                        <h6>{{ $errors->first('email') }}</h6>
                      </span>
                      @endif

                    </div>
                  </div>

                   <div class="form-group">
                    <div class="input-group">
                      <input type="text" class="form-control" name="contact" placeholder="Enter Phone Number" required="required" maxlength="10">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-phone"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" required="required">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-lock"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" required="required">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-lock"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                
                <div class="input-group">
                  <input type="file" class="form-control" name="picture" placeholder="Profile photo">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="fa fa-image"></i>
                    </span>
                  </div>
                </div><hr>

                <input type="checkbox" onclick="myFunction()"> Show Password
                
            </div>



            <div class="modal-footer">
              <div class="form-group">
                <button type="submit" class="btn btn-success submit-btn btn-lg btn-block">
                  <i class="menu-icon fas fa-save"></i> Save
                </button>
              </div>
            </div>

        </form>
     </div>
  </div>
</div>

@include('partials.footer')
</div>

@endsection

<script type="text/javascript">
 
function myFunction() {
  var x = document.getElementById("password");
  var y = document.getElementById("password_confirmation");

  if (x.type === "password" || y.type === "password") {
    x.type = "text";
    y.type = "text";

  } else {
    x.type = "password";
    y.type = "password";
  }
}

</script>