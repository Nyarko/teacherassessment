@extends('layouts.adminLayout')

@section('content')

<div class="main-panel">
<div class="content-wrapper">
  
  <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-cube text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Total Downloads</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ $totalDownload }}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-receipt text-warning icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Total Request</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{  $totalCustomer }}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-poll-box text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Users</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ $totalUsers }}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account-location text-info icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Comments</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{  $totalComments }}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('partials.alert')
  <div class="row">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">All Requests</h4>
          <div class="table-responsive">
            <table class="table table-bordered" id="myTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Full Name</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>User Type</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($customers as $id => $customer)
                  <tr>
                  <td class="font-weight-medium">{{ $id += 1}}</td>
                  <td>{{ ucfirst(trans( $customer->name ))}}</td>
                  <td>{{ $customer->contact }}</td>
                  <td>{{ $customer->email }}</td>
                  <td class="text-danger">{{ ucfirst(trans( $customer->user_type ))}}</td>
                  <td>{{ date('M j, Y', strtotime($customer->created_at )) }}</td>
                  <td style="text-align: center"><a href="{{ route('del_customer', [$id => $customer->id]) }}" class="text-danger" title="click to delete"><i class="fa fa-trash"></i></a></td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
 @include('partials.footer')
</div>

@endsection
