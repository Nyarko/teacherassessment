@if (session('error'))
  <div class="alert alert-danger" id="myAlert">
      {{ session('error') }}
  </div>
@endif

@if (session('customer_request'))
  <div class="alert alert-success" id="myAlert">
      {{ session('customer_request') }}
  </div>
@endif

@if (session('comment'))
  <div class="alert alert-success" id="myAlert">
      {{ session('comment') }}
  </div>
@endif

@if (session('create-user-success'))
  <div class="alert alert-success" id="myAlert">
      {{ session('create-user-success') }}
  </div>
@endif

@if (session('user_del'))
  <div class="alert alert-success" id="myAlert">
      {{ session('user_del') }}
  </div>
@endif

@if (session('update-user-success'))
  <div class="alert alert-success" id="myAlert">
      {{ session('update-user-success') }}
  </div>
@endif

@if (session('pass-change'))
  <div class="alert alert-success" id="myAlert">
      {{ session('pass-change') }}
  </div>
@endif

@if (session('article-success'))
  <div class="alert alert-success" id="myAlert">
      {{ session('article-success') }}
  </div>
@endif

@if (session('update-article'))
  <div class="alert alert-success" id="myAlert">
      {{ session('update-article') }}
  </div>
@endif