<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">

  <div class="navbar-brand-wrapper d-flex align-items-top justify-content-center">
    <a class="navbar-brand brand-logo" href="{{ route('admin_index') }}" style="color: #7fb401">
     <span style="font-size: 20px;">Assessment</span>
    </a>
    <a class="navbar-brand brand-logo-mini" href="{{ route('admin_index') }}" style="color: #7fb401">
       <span style="font-size: 18px; font-weight: bolder;">A</span><span style="font-weight: bolder; font-size: 25px;">4</span><span style="font-size: 20px; font-weight: bolder;">F</span>
    </a>
  </div>

  <div class="navbar-menu-wrapper d-flex align-items-center">

    <ul class="navbar-nav navbar-nav-right">

      <li class="nav-item dropdown d-xl-inline-block">
        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          @if(Auth::check())
            <span class="profile-text"> {{ Auth::user()->name }} </span>
            <img class="img-xs rounded-circle" src="{{ asset('/images/users/'.Auth::user()->avatar) }}" alt="Profile image">
          @endif
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
           <a href="" class="dropdown-item mt-2" data-toggle="modal" data-target="#profile">
              <i class="fa fa-user"></i> View Profile
            </a>
            <a href="" class="dropdown-item" data-toggle="modal" data-target="#changepass">
               <i class="fa fa-sun"></i>  Change Password
            </a>
            <a  class="dropdown-item" href="{{ route('logout') }}">
               <i class="fa fa-sign"></i>  Sign Out
            </a>
        </div>
      </li>
    </ul>

    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="mdi mdi-menu"></span>
    </button>
  </div>
</nav>

@if(Auth::check())

 <div id="profile" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="background-color:white">
       <form action="{{ route('update_user', [Auth::user()->id]) }}" method="post" enctype="multipart/form-data">
           {{ csrf_field() }}
           <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">

          <div class="modal-header">
             <h4 class="modal-title">Profile Details</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

           <div class="modal-body">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="auto-form-wrapper">

                  <h4 class="text-center mb-4">
                   <img class="img-xs rounded-circle" src="{{ asset('/images/users/'.Auth::user()->avatar) }}" alt="Profile image">
                  </h4>
                  {{ csrf_field() }}

                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" placeholder="Name" required="required">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-user"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="email" class="form-control {{ $errors->has('email') ? 'has-error' : '' }}" name="email" value="{{ Auth::user()->email }}" placeholder="Email" required="required">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-envelope"></i>
                        </span>
                      </div>

                      @if ($errors->has('email'))
                      <span class="help-block text-danger">
                        <h6>{{ $errors->first('email') }}</h6>
                      </span>
                      @endif

                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" class="form-control" name="contact" value="{{ Auth::user()->contact }}" placeholder="Phone Number" required="required" maxlength="12">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-phone"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="file" class="form-control" name="picture" placeholder="Profile photo">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-image"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
               </div>
             </div>

              <div class="modal-footer">
               <div class="form-group">
                    <button type="submit" class="btn btn-success submit-btn"><i class="menu-icon fas fa-save"></i> Update Info</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
        </form>
     </div>
  </div>
 </div>

@endif

@if(Auth::check())

 <div id="changepass" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="background-color:white">
       <form action="{{ route('update_password', [Auth::user()->id]) }}" method="post" enctype="multipart/form-data">
           
           <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">

          <div class="modal-header">
             <h4 class="modal-title">Change Password</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

           <div class="modal-body">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="auto-form-wrapper">
                
                  {{ csrf_field() }}

                  <div class="form-group">
                    <div class="input-group">
                      <input type="email" class="form-control {{ $errors->has('email') ? 'has-error' : '' }}" name="email" value="{{ Auth::user()->email }}" placeholder="Email" required disabled>
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-envelope"></i>
                        </span>
                      </div>

                      @if ($errors->has('email'))
                      <span class="help-block text-danger">
                        <h6>{{ $errors->first('email') }}</h6>
                      </span>
                      @endif

                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <input type="password" class="form-control" name="password" id="password" placeholder="Enter New Password" required="required" minlength="6">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-key"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                   <div class="form-group">
                    <div class="input-group">
                      <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm New Password" required="required" minlength="6">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-key"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <input type="checkbox" onclick="myFunction()"> Show Password

                </div>
               </div>
             </div>

              <div class="modal-footer">
               <div class="form-group">
                    <button type="submit" class="btn btn-success submit-btn"><i class="menu-icon fas fa-save"></i> Change Password</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
        </form>
     </div>
  </div>
</div>
@endif

<script type="text/javascript">
 
function myFunction() {
  var x = document.getElementById("password");
  var y = document.getElementById("password_confirmation");

  if (x.type === "password" || y.type === "password") {
    x.type = "text";
    y.type = "text";

  } else {
    x.type = "password";
    y.type = "password";
  }
}

</script>
