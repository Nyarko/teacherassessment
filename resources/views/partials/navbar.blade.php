<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">

     <li class="nav-item">
      <a class="nav-link" href="{{ route('admin_index')}}" target="_SELF">
        <i class="menu-icon fas fa-home text-dark"></i>
        <span class="menu-title">DashBoard</span>
      </a>
    </li>
    
    <li class="nav-item">
      <a class="nav-link" href="{{ route('all_user')}}" target="_SELF">
        <i class="menu-icon fas fa-user text-success"></i>
        <span class="menu-title">Users</span>
      </a>
    </li>

     <li class="nav-item">
      <a class="nav-link" href="{{ route('all_comment')}}" target="_SELF">
        <i class="menu-icon fas fa-comment text-info"></i>
        <span class="menu-title">Comments</span>
      </a>
    </li>

     <li class="nav-item">
      <a class="nav-link" href="{{ route('all_article')}}" target="_SELF">
        <i class="menu-icon fas fa-briefcase text-danger"></i>
        <span class="menu-title">Articles</span>
      </a>
    </li>

  </ul>
</nav>