<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('js/datatables.js') }}" defer></script>

<script type="text/javascript" src="{{ asset('js/vendor.bundle.base.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('js/vendor.bundle.addons.js') }}" defer></script>

<script type="text/javascript" src="{{ asset('js/off-canvas.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('js/dashboard.js') }}" defer></script>

<script type="text/javascript" src="{{ asset('js/custom.js') }}" defer></script>

<script type="text/javascript" src="{{ asset('js/misc.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}" defer></script>
