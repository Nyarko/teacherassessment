<link rel="stylesheet" href="{{ asset('css/datatables.css') }}" defer/>
<link rel="stylesheet" href="{{ asset('css/materialdesignicons.min.css') }}" defer>
<link rel="stylesheet" href="{{ asset('css/vendor.bundle.base.css') }}" defer>
<link rel="stylesheet" href="{{ asset('css/vendor.bundle.addons.css') }}" defer>

<link href="{{ asset('css/all.css') }}" rel="stylesheet" defer> 

<link rel="stylesheet" href="{{ asset('css/style.css') }}" defer>

