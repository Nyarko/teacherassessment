<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Resources\UserResource;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\CommentResource;
use App\Http\Resources\ArticleResource;
use App\Customer;
use App\User;
use App\Comment;
use App\Article;
use Validator;
use Image;
use Session;
use DB;

class AllController extends Controller
{
    //======== User Section =================
    public function getUsers(){
        $users = User::orderBy('created_at','desc')->get();
        return UserResource::collection($users);
    }

    public function saveUser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'contact' => 'required|string|max:10',
            'password' => 'required|string',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
           return redirect()->back()->with('error','Check and Fill all required fields');
        }

        $emailcom = $request['email'];

        if (DB::table('users')->where('email','=', $emailcom)->count() > 0) {
            return redirect()->back()->with('error','Ooop!!! Email has been registered');
        } else {

           $user = new User;
           $user->name = $request['name'];
           $user->email = $emailcom;
           $user->contact = $request['contact'];
           $user->password = bcrypt($request['password']);

          if($request->hasFile('picture')){
              $picture  = $request->file('picture');
              $filename = time().$picture->getClientOriginalName();
              Image::make($picture)->fit(300,300)->save( public_path('/images/users/' . $filename));
              $user->avatar=$filename;
          }else {
              $user->avatar = 'avatar.png';
          }

          if($user->save()){
            Session::flash('create-user-success', 'User Created Successfully!');
            return new UserResource($user);
          }
        }
    }

    public function userInfo(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'email'=>'required|email',
            'contact'=>'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error','Check and Fill all required fields');
        } else {

           $user = User::findorFail($request['user_id']);
           $user->name = $request['name'];
           $user->email = $request['email'];
           $user->contact = $request['contact'];

           if($request->hasFile('picture')){
              $picture  = $request->file('picture');
              $filename = time().$picture->getClientOriginalName();
              Image::make($picture)->fit(300,300)->save( public_path('/images/users/' . $filename));
              $user->avatar=$filename;
            }

           if($user->save()){
              Session::flash('update-user-success', 'Profile Information Updated Successfully!');
              return new UserResource($user);
            }
        }
    }


    public function changePassword(Request $request){
      $validator = Validator::make($request->all(), [
            'password'=>'required|confirmed|string|min:6',
      ]);

      if ($validator->fails()) {
           return redirect()->back()->with('error', 'Ooops!!! Please check and confirm password');
      } else {

           $user = User::findorFail($request['user_id']);
           $user->password = bcrypt($request['password']);

           if($user->save()){
              Session::flash('pass-change', 'Password changed Successfully!');
              return new UserResource($user);
           }

      }
    }

    public function destroyUser($id){
       $user = User::findOrFail($id);
       if ($user->delete()){
         return new UserResource($user);
       }
    }

    public function getOneUser($id){
       $user = User::findOrFail($id);
       if ($user){
         return new UserResource($user);
       }
    }

    //=========== End User Section ===================

    //=========== Customer Starts Here ===============

    public function getCustomers(){
        $customers = Customer::orderBy('created_at','desc')->get();
        return CustomerResource::collection($customers);
    }

    public function saveCustomer(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'contact' => 'required|string|max:10',
            'email' => 'required|email',
            'user_type' => 'required|string',
        ]);

        if ($validator->fails()) {
           return redirect()->back()->with('error','Check and Fill all required fields');
        }

         $customer = new Customer;
         $customer->name = $request['name'];
         $customer->email = $request['email'];
         $customer->contact = $request['contact'];
         $customer->user_type = $request['user_type'];

         if($customer->save()){
            Session::flash('customer_request', 'Request send Successfully!');
            return new CustomerResource($customer);
         }
    }

    public function destroyCustomer($id){
        $customer = Customer::findOrFail($id);
       if ($customer->delete()){
         return new CustomerResource($customer);
       }
    }

    //=========== End Customer =======================

   //======== Comment Starts here ====================
    public function getComment(){
        $comments = Comment::orderBy('created_at','desc')->get();
        return CommentResource::collection($comments);
    }

    public function saveComment(Request $request){

        $validator = Validator::make($request->all(), [
            'features' => 'required|string|max:100',
            'comment' => 'required|string',
        ]);

        if ($validator->fails()) {
           return redirect()->back()->with('error','Check and Fill all required fields');
        }

        $comment = new Comment;
        $comment->features = $request['features'];
        $comment->comment = $request['comment'];

        if($comment->save()){
          Session::flash('comment', 'Comment send, Thank You');
          return new CommentResource($comment);
        }
    }

    public function destroyComment($id){
       $comment = Comment::findOrFail($id);
       if ($comment->delete()){
         return new CommentResource($comment);
       }
    }
    //=========End Comment ===========================


    //======= Article Section ========================
    // Article
    public function getArticles(){
        $article = Article::all();
        return ArticleResource::collection($article);
    }

    public function addArticle(Request $request){
        $validator = Validator::make($request->all(), [
            'title'=>'required',
            'body'=>'required',
        ]);

        if ($validator->fails()) {
           return redirect()->back()->with('error','Check and Fill all required fields');
         } else{

           $article = new Article;
           $article->title = $request['title'];
           $article->body = $request['body'];
           $article->link = $request['link'];

           if($request->hasFile('picture')){
              $picture  = $request->file('picture');
              $filename = time().$picture->getClientOriginalName();
              Image::make($picture)->save( public_path('/images/software/'. $filename));
              $article->avatar=$filename;
            }else {
              $article->avatar = 'avatar.png';
            }

           if($article->save()){
              Session::flash('article-success', 'Article Saved Successfully!');
              return new ArticleResource($article);
           }
       }
    }

    public function updateArticle(Request $request){

      $validator = Validator::make($request->all(), [
            'title'=>'required',
            'body'=>'required',
      ]);

      if ($validator->fails()) {
         return redirect()->back()->with('error','Check and Fill all required fields');
      } else{

         $article = Article::findorFail($request['user_id']);
         $article->title = $request['title'];
         $article->body = $request['body'];
         $article->link = $request['link'];

        if($request->hasFile('picture')){
            $picture  = $request->file('picture');
            $filename = time().$picture->getClientOriginalName();
            Image::make($picture)->save( public_path('/images/software/'. $filename));
            $article->avatar=$filename;
         }

        if($article->save()){
           Session::flash('update-article', 'Article Updated Successfully!');
           return new ArticleResource($article);
         }

       }
    }

    public function destroyArticle($id){
       $article = Article::findOrFail($id);
       if ($article->delete()){
         return new ArticleResource($article);
       }
    }

    ///==================== End Article Here ==========
}
