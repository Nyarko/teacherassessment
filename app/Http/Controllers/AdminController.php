<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use Validator;
use Session;
use App\Comment;
use App\Download;
use App\Article;
use Image;
use Auth;
use Response;
use App\Download as UserDownload;


class AdminController extends AllController
{

    public function indexPage(){
        $articles = parent::getArticles();
        return view('pages.index', compact('articles'));
    }

    public function indexAdmin(){
        $customers = parent::getCustomers();
        $users = parent::getUsers();
        $comments = parent::getComment();

        $totalCustomer = $customers->count();
        $totalUsers = $users->count();
        $totalDownload = Download::get()->count();
        $totalComments = $comments->count();
    	return view('admin.index', compact('customers', 'totalCustomer', 'totalUsers', 'totalDownload', 'totalComments'));
    }

    ///============= User Starts Here ================
    //get all users
    public function allUsers(){
        $users = parent::getUsers();
    	  return view('admin.users', compact('users'));
    }

    //saving user into db
    public function saveUser(Request $request){
        $user = parent::saveUser($request);
        if(!$user){
           return redirect()->back()->with('error', 'User Not Created!');
        }
         return redirect()->back();
    }

    //Updating user infomation
    public function userInfo(Request $request){
       $info = parent::userInfo($request);
       if(!$info){
         return redirect()->back()->with('error', 'Update Failed');
       }
       return redirect()->back();
    }

    //change user password
    public function changePassword(Request $request){
        $user = parent::changePassword($request);
        if(!$user){
         return redirect()->back()->with('error', 'Ooops!!! Please check and confirm password');
        }
        return redirect()->back();
    }

    //deleting user
    public function delUser($id){
      $deluser = parent::destroyUser($id);
      if($deluser){
           Session::flash('user_del', 'User is deleted Successfully');
           return redirect()->back();
      }
    }

      //login section
    public function loginUser(Request $request){
        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember)){
          return redirect()->route('admin_index');
          }else{
          return redirect()->back()->with(['error'=>'Invalid Login Credentials']);
        }
    }

    //========= End User =============================

    //========= Customer Starts Here =================

    //Registering customer
    public function saveCustomer(Request $request){
        $customer = parent::saveCustomer($request);
        if($customer){
          return redirect()->back()->with('error', 'Request was not successful!');
        }
          return redirect()->back();
    }

    //deleting customer
    public function delCustomer($id){
      $delcustomer = parent::destroyCustomer($id);
      if($delcustomer){
        return redirect()->back()->with('user_del', 'Customer is deleted Successfully');
      }
    }

    //======== End of Customer ========================

    //========= Download Section ==========================
    //Function for the download of 64 bit windows
    public function downloadApp(){
       $file = public_path()."/setups/Assessment.rar";
       $headers = array('Content-Type: application/rar',);

       $down = new UserDownload;
       $down->quant = 1;
       $down->save();

       return Response::download($file, "Assessment.rar", $headers);
    }

    //Function for the download 32 bit windows
    public function downloadAppTwo(){
       $file = public_path()."/setups/Assessment32.rar";
       $headers = array('Content-Type: application/rar',);

       $down = new UserDownload;
       $down->quant = 1;
       $down->save();

       return Response::download($file, "Assessment32.rar", $headers);
    }
    //========== End Section ===============================

    //============= Comments Starts Here ===================
    //get all comments
    public function allComments(){
      $comments = parent::getComment();
      return view('admin.allComment', compact('comments'));
    }

    //Registering customer
    public function saveComment(Request $request){
       $comment = parent::saveComment($request);
         if(!$comment){
          return redirect()->back()->with('error', 'Comment was not send');
         }
        return redirect()->back();
    }

    public function delComment($id){
      $delcomment = parent::destroyComment($id);
      if($delcomment){
        Session::flash('user_del', 'Comment is deleted Successfully');
        return redirect()->back();
      }
    }

    //============== End Comment ==============================

  /// Other functions
	public function logOut(){
	    Auth::logout();
	    return redirect()->route('index');
	}

	public function loginNotPermited(){
		return view('pages.login');
	}

  //=============== Article Section Starts Here ==============

  // aArticle section

  public function articlePage(){
     $articles = parent::getArticles();
     return view('admin.article', compact('articles'));
  }

  public function addArticle(Request $request){
      $article = parent::addArticle($request);
      if(!$article){
         return redirect()->back()->with('error','Article was not saved');
      }
      return redirect()->back();
  }

  public function updateArticle(Request $request){
        $article = parent::updateArticle($request);
        if(!$article){
           return redirect()->back()->with('error', 'Article not updated');
        }
        return redirect()->back();
  }

  public function delArticle($id){
      $delarticle = parent::destroyArticle($id);
      if($delarticle){
           Session::flash('user_del', 'Article is deleted Successfully');
           return redirect()->back();
      }
  }

  ///=============== Article Ends Here ======================

}
