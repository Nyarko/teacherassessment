<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CommentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'features' => $this->features,
            'comment' => $this->comment,
            'created_at' => date('M j, Y', strtotime($this->created_at)),
        ];
    }
}
