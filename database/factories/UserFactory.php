<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => 'Nyarko Isaac Kwadwo',
        'email' => 'nyarkorep@gmail.com',
        'contact' => '0500383888',
        'password' => bcrypt('@Nyarko2018'),
        'avatar' => 'avatar.png',
    ];
});

// $factory->define(App\Customer::class, function (Faker $faker) {
//     return [
//         'name' => $faker->name,
//         'email' => $faker->unique()->safeEmail,
//         'contact' => $faker->phoneNumber,
//         'user_type' => $faker->randomElement(['self', 'school']),
//     ];
// });
//
// $factory->define(App\Comment::class, function (Faker $faker) {
//     return [
//         'features' => $faker->word,
//         'comment' => $faker->sentence,
//     ];
// });
//
// $factory->define(App\Download::class, function (Faker $faker) {
//     return [
//         'quant' => $faker->numberBetween(1,10),
//     ];
// });
