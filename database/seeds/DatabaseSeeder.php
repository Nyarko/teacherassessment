<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class, 1)->create();
        // factory(App\Customer::class, 30)->create();
        // factory(App\Download::class, 30)->create();
        // factory(App\Comment::class, 30)->create();
    }
}
