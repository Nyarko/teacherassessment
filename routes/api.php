<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware'=>'auth'],function(){
   
});

// All GET Request

Route::get('users','AllController@getUsers');

Route::get('/articles', 'AllController@getArticles');

Route::get('/comments', 'AllController@getComment');

Route::get('/customers', 'AllController@getCustomers');

Route::get('user/{id}', 'AllController@getOneUser');

// All Post Request

Route::post('/user', 'AllController@saveUser');

Route::post('/article', 'AllController@addArticle');

Route::post('/comment', 'AllController@saveComment');

Route::post('/customer', 'AllController@saveCustomer');


// All delete Request

Route::delete('/user/{id}', 'AllController@destroyUser');

Route::delete('/article/{id}', 'AllController@destroyArticle');

Route::delete('/comment/{id}', 'AllController@destroyComment');

Route::delete('/customer/{id}', 'AllController@destroyCustomer');


//All Update Request
Route::post('user/update', 'AllController@userInfo');

Route::post('article/{id}', 'AllController@updateArticle');
