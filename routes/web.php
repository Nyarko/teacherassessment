<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','AdminController@indexPage')->name('index');

Route::get('logout', 'AdminController@logOut')->name('logout');

Route::post('/login/user', 'AdminController@loginUser')->name('login_user');

Route::get('/login', 'AdminController@loginNotPermited')->name('login');

Route::post('/customer/saved', 'AdminController@saveCustomer')->name('save_customer');

Route::post('/comment/send', 'AdminController@saveComment')->name('save_comment');

Route::get('/download', 'AdminController@downloadApp')->name('setup_download');

Route::get('/download/two', 'AdminController@downloadAppTwo')->name('setup_download_two');



Route::group(['middleware' => ['auth', 'admin']], function(){

//getting	

Route::get('/admin', 'AdminController@indexAdmin')->name('admin_index');

Route::get('/all/users', 'AdminController@allUsers')->name('all_user');

Route::get('/all/comments', 'AdminController@allComments')->name('all_comment');

Route::get('/article', 'AdminController@articlePage')->name('all_article');

//posting

Route::post('/user/saved', 'AdminController@saveUser')->name('save_user');

Route::post('/profile/update/{id}', 'AdminController@userInfo')->name('update_user');

Route::post('/change/password/{id}', 'AdminController@changePassword')->name('update_password');

Route::post('/article/saved', 'AdminController@addArticle')->name('article_user');

Route::post('/article/update/{id}', 'AdminController@updateArticle')->name('update_article');

//deleting
Route::get('/user/delete/{id}', 'AdminController@delUser')->name('del_user');

Route::get('/customer/delete/{id}', 'AdminController@delCustomer')->name('del_customer');

Route::get('/comment/delete/{id}', 'AdminController@delComment')->name('del_comment');

Route::get('/article/delete/{id}', 'AdminController@delArticle')->name('del_article');

});


